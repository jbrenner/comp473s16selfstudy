package shapes;

/**
 * Created by Brenner on 4/5/2016.
 */
public class Circle implements Shape {
    int r;

    public Circle(int r) {
        this.r = r;
    }

    public int getR() {
        return r;
    }

    public void setR(int r) {
        this.r = r;
    }
}
