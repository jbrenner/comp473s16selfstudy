package shapes;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by Brenner on 4/5/2016.
 */
public class Shapes implements Shape {
    List<Shape> shapes;

    public Shapes(final Shape... shapes) {
        this.shapes = Arrays.asList(shapes);
    }

    public List<Shape> getShapes() {
        return Collections.unmodifiableList(shapes);
    }


}
