package shapes;

/**
 * Created by Brenner on 4/5/2016.
 */
public class Location implements Shape{
    int x, y;
    Shape shape;

    public Location(int x, int y, Shape shape) {
        this.x = x;
        this.y = y;
        this.shape = shape;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }
}
