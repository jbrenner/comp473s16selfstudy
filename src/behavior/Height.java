package behavior;

import shapes.*;

/**
 * Created by Brenner on 4/5/2016.
 */
public class Height {


    public int apply(Shape s) {

        if (s instanceof Shapes) {
            for (Shape l : ((Shapes) s).getShapes()) return apply(l)+1;
        }
        if (s instanceof Rectangle)
            return 1;
        else if (s instanceof Circle)
            return 1;
        else if (s instanceof Location)
            return apply(((Location) s).getShape()) + 1;
        else return 0;
    }

}
