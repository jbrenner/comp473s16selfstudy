package test;

import behavior.Height;
import org.junit.Test;

import static org.junit.Assert.*;


public class behaviorTest {
    Height z = new Height();

    /** Test for number of nodes in Shape tree */
    @Test
    public void testHeight(){
        assertEquals(1,z.apply((fixtures.simpleRectangle)));
        assertEquals(1,z.apply((fixtures.simpleCircle)));
        assertEquals(2,z.apply((fixtures.simpleLocation)));
        assertEquals(3,z.apply((fixtures.simpleGroup)));
        assertEquals(4,z.apply((fixtures.complexGroup)));


    }



}