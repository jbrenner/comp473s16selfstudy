#Readme 

Comp473s16 self-study assignment. 

Patterns implemented:
* Composite
* *  `Shapes.java`
* Decorator
* *`Location.java`

Run via unit tests found in `behaviorTests.java`
 * test fixtures found in `fixtures.java`